// GCD implemented in functional style

fn abs(x: i64) -> i64 {
    // return the absolute value
    if x >= 0 {
        return x;
    } else {
        return -1 * x;
    }
}


fn bigger(a: i64, b: i64) -> i64 {
    // return the bigger number
    if a >= b {
        return a;
    } else {
        return b;
    }
}


fn smaller(a: i64, b: i64) -> i64 {
    // return the smaller number
    return -1 * bigger(-1*a, -1*b);
}


fn gcd(a: i64, b: i64) -> i64 {
    // this function is responsible for making sure the inputs to
    // gcd_abs are positive
    let abs_a: i64 = abs(a);
    let abs_b: i64 = abs(b);

    return gcd_abs(abs_a, abs_b);
}


fn gcd_abs(a: i64, b: i64) -> i64 {
    // this function is responsible for making sure the inputs to
    // gcd_plus are in the correct order
    let bigger: i64 = bigger(a, b);
    let smaller: i64 = smaller(a, b);

    return gcd_plus(bigger, smaller);
}


fn gcd_plus(a: i64, b: i64) -> i64 {
    // we now get to assume both inputs are non-negative, and a >= b
    if b == 0 {
        return a;
    } else {
        let r: i64 = a % b;
        return gcd_plus(b, r);
    }
}


fn main() {
    let mut a: i64;
    let mut b: i64;

    a = 60;
    b = 22;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = 11;
    b = 22;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = 2;
    b = 4;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = -60;
    b = 105;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));
}

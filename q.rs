// rational number arithmetic half-example in rust
// implemented with 8-bit integers just to show the issue

// kind of like an import I gather
mod gcd8;

// This is so we can print the Q struct easily
// equivalent to "enabling" ~p formatting in Erlang
#[derive(Debug)]
// apparently in Rust you have to specify that you can make copies of
// things...
#[derive(Clone)]
#[derive(Copy)]

struct Q {
    top: i8,
    bot: i8,
}

fn q(t: i8, b: i8) -> Q {
    // this eliminates common divisors between top and bottom
    // (reduces the fraction), and makes sure that negative fractions
    // have the top integer be negative, and the bottom integer be
    // positive.
    if b == 0 {
        // can't divide by zero
        panic!("you can't divide by zero, you asshole");
    } else if b == 1 {
        // if the bottom of the fraction is 1, we don't need to do
        // that gcd stuff.
        //
        // nothing would break if we didn't explicitly check this
        // case, but eh good for clarity I susppose
        //
        // the reason it wouldn't break is because the gcd of
        // anything with 1 is 1, so the code in the else {...} block
        // would just divide top and bot by 1 (doing nothing)
        return Q{top: t, bot: b};
    } else if b < 0 {
        // the signed part goes in the top of the fraction, so let us
        // do a re-entry call
        return q(-1 * t, -1 * b);
    } else {
        // at this point, we can assume that bot is strictly positive
        // divide both top and bottom by the gcd to reduce
        let gcd   : i8 = gcd8::gcd(t, b);
        let new_t : i8 = t / gcd;
        let new_b : i8 = b / gcd;
        return Q{ top: new_t, bot: new_b };
    }
}


fn square(rat: Q) -> Q {
    let t : i8 = rat.top;
    let b : i8 = rat.bot;

    return Q{ top: t*t, bot: b*b };
}


fn main() {
    let mut a: i8;
    let mut b: i8;

    a = 60;
    b = 22;
    println!("q({}, {}) = {:?}", a, b, q(a, b));

    a = 11;
    b = 22;
    println!("q({}, {}) = {:?}", a, b, q(a, b));

    a = 2;
    b = 4;
    println!("q({}, {}) = {:?}", a, b, q(a, b));

    a = -60;
    b = 105;
    println!("q({}, {}) = {:?}", a, b, q(a, b));

    a = -60;
    b = 107;
    println!("q({}, {}) = {:?}", a, b, q(a, b));


    // Let's try squaring
    let rat: Q;
    let rat_squared: Q;

    rat         = q(a, b);
    rat_squared = square(rat);

    println!("rat         = {:?}", rat);
    println!("rat_squared = {:?}", rat_squared);
}

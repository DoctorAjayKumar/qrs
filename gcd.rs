// GCD implemented in imperative style


fn abs(x: i64) -> i64 {
    // return the absolute value
    if x >= 0 {
        return x;
    } else {
        return -1 * x;
    }
}


fn bigger(a: i64, b: i64) -> i64 {
    // return the bigger number
    if a >= b {
        return a;
    } else {
        return b;
    }
}


fn smaller(a: i64, b: i64) -> i64 {
    // return the smaller number
    return -1 * bigger(-1*a, -1*b);
}


fn gcd(a: i64, b: i64) -> i64 {
    // don't want the headache of negative numbers
    let abs_a: i64 = abs(a);
    let abs_b: i64 = abs(b);

    // need to define 3 variables
    let mut bigger  : i64 = bigger(abs_a, abs_b);
    let mut smaller : i64 = smaller(abs_a, abs_b);
    let mut rem     : i64;

    // keep doing remainders until we get a remainder of 0
    while smaller != 0 {
        rem     = bigger % smaller;
        bigger  = smaller;
        smaller = rem;
    }

    return bigger;
}

fn main() {
    let mut a: i64;
    let mut b: i64;

    a = 60;
    b = 22;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = 11;
    b = 22;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = 2;
    b = 4;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = -60;
    b = 105;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));
}

// GCD implemented in functional style, but with 8-bit integers

// interface
pub fn gcd(a: i8, b: i8) -> i8 {
    // this function is responsible for making sure the inputs to
    // gcd_abs are positive
    let abs_a: i8 = abs(a);
    let abs_b: i8 = abs(b);

    return gcd_abs(abs_a, abs_b);
}


fn abs(x: i8) -> i8 {
    // return the absolute value
    if x >= 0 {
        return x;
    } else {
        return -1 * x;
    }
}


fn bigger(a: i8, b: i8) -> i8 {
    // return the bigger number
    if a >= b {
        return a;
    } else {
        return b;
    }
}


fn smaller(a: i8, b: i8) -> i8 {
    // return the smaller number
    return -1 * bigger(-1*a, -1*b);
}


fn gcd_abs(a: i8, b: i8) -> i8 {
    // this function is responsible for making sure the inputs to
    // gcd_plus are in the correct order
    let bigger: i8 = bigger(a, b);
    let smaller: i8 = smaller(a, b);

    return gcd_plus(bigger, smaller);
}


fn gcd_plus(a: i8, b: i8) -> i8 {
    // we now get to assume both inputs are non-negative, and a >= b
    if b == 0 {
        return a;
    } else {
        let r: i8 = a % b;
        return gcd(b, r);
    }
}


// turn off that dumbass warning about unused main
#[allow(dead_code)]
fn main() {
    let mut a: i8;
    let mut b: i8;

    a = 60;
    b = 22;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = 11;
    b = 22;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = 2;
    b = 4;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));

    a = -60;
    b = 105;
    println!("gcd({}, {}) = {}", a, b, gcd(a, b));
}
